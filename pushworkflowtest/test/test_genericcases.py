# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2017-2019 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/
__authors__ = ["H. Payno"]
__license__ = "MIT"
__date__ = "24/06/2019"


import unittest
from pushworkflowtest.pushworkflow.scheme import scheme, node, link
from pushworkflowtest.pushworkflow.main import exec_


def mycallback(input_):
    """Simple callback function for test"""
    if input_:
        input_.update({'called': True})
        assert isinstance(input_, dict)
    else:
        return {'called': True}
    return input_


class MyCallable(object):
    """Simple callable class"""
    def __call__(self, input_):
        return {'called': True}


class MyObject(object):
    """Simple object with handler"""
    inputs = [
        ("mydata", dict, "handler_1"),
        ("mydata2", dict, "handler_2"),
    ]

    def handler_1(self, input_):
        input_.update({'called': True})
        return ('mydata2', input_)

    def handler_2(self, input_):
        input_.update({'also called': True})
        return input_

    def process(self, input_=None):
        return ('mydata', {'started': True})


class TestCallbackCases(unittest.TestCase):
    """Make sure the process have valid io"""

    def test_nodes_fct_callback(self):
        """simple scheme with two nodes. Callback is a function"""
        node1 = node.Node(callback=mycallback)
        my_scheme = scheme.Scheme(nodes=[node1, ])
        # with TestLogging(process_logger, info=1):
        out_ = exec_(scheme=my_scheme)
        self.assertTrue('called' in out_)

    def test_nodes_callable_obj(self):
        """simple scheme with two nodes. Callback is a callable object"""
        node1 = node.Node(callback='.'.join((__file__, 'MyCallable')))
        my_scheme = scheme.Scheme(nodes=[node1, ])
        out_ = exec_(scheme=my_scheme)
        self.assertTrue('called' in out_)

    def test_nodes_obj_handler_function(self):
        """simple scheme with two nodes. Callback is a specific function of an
        object to instantiate
        """
        node1 = node.Node(callback='.'.join((__file__, 'MyObject')))
        node2 = node.Node(callback='.'.join((__file__, 'MyObject')))
        node3 = node.Node(callback='.'.join((__file__, 'MyObject')))

        link1 = link.Link(node1, node2, 'mydata', 'mydata')
        link2 = link.Link(node2, node3, 'mydata2', 'mydata2')

        nodes = [node1, node2, node3]
        links = [link1, link2]

        my_scheme = scheme.Scheme(nodes=nodes, links=links)
        # with TestLogging(obj_logger, critical=1, warning=1, info=1):
        out_ = exec_(scheme=my_scheme)
        self.assertTrue('started' in out_)
        self.assertTrue('called' in out_)
        self.assertTrue('also called' in out_)


class TestGenericCases(unittest.TestCase):
    def two_nodes_into_one(self):
        """Two nodes are triggering one node with different handler"""
        pass


def suite():
    test_suite = unittest.TestSuite()
    for ui in (TestCallbackCases, TestGenericCases, ):
        test_suite.addTest(unittest.defaultTestLoader.loadTestsFromTestCase(ui))
    return test_suite


if __name__ == '__main__':
    unittest.main(defaultTest="suite")
